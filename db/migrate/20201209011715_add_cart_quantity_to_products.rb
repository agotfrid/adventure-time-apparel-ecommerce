class AddCartQuantityToProducts < ActiveRecord::Migration[6.0]
  def change
    add_column :products, :cart_quantity, :integer
  end
end
