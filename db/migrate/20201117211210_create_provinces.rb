class CreateProvinces < ActiveRecord::Migration[6.0]
  def change
    create_table :provinces do |t|
      t.string :province_name
      t.string :gst_rate
      t.string :pst_rate

      t.timestamps
    end
  end
end
