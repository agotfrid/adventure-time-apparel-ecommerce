class AddProvinceCodeToProvinces < ActiveRecord::Migration[6.0]
  def change
    add_column :provinces, :province_code, :string
  end
end
