class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
      t.string :shipping_address
      t.float :order_cost
      t.float :shipping_cost
      t.float :gst_amount
      t.float :pst_amount
      t.references :customer, null: false, foreign_key: true

      t.timestamps
    end
  end
end
