class AddDescriptionToProducts < ActiveRecord::Migration[6.0]
  def change
    add_column :products, :product_description, :text
  end
end
