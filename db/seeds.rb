ProductImage.destroy_all
Product.destroy_all
# OrderProduct.destroy_all
# Order.destroy_all
# Customer.destroy_all
# Province.destroy_all
Category.destroy_all
require "faker"
require "open-uri"
require "json"
require "csv"

provinces = { AB: "Alberta",
              BC: "British Columbia",
              MB: "Manitoba",
              NB: "New Brunswick",
              NL: "Newfoundland and Labrador",
              NS: "Nova Scotia",
              NU: "Northwest Territories",
              NT: "Nunavut",
              ON: "Ontario",
              PE: "Prince Edward Island",
              QC: "Quebec",
              SK: "Saskatchewan",
              YT: "Yukon" }

# Create Provinces
# provinces.each do |province_code, province_name|
#   tax_response = JSON.parse(
#     URI.open("https://api.salestaxapi.ca/v2/province/#{province_code}").read
#   )
#   Province.create!(
#     province_code: province_code,
#     province_name: province_name,
#     gst_rate:      tax_response["gst"],
#     pst_rate:      tax_response["pst"]
#   )
#   puts "Using #{province_code} for #{province_name}"
# end

CSV.foreach(Rails.root.join("db/scraped_data/all_camp_and_hike.csv"), headers: true) do |row|
  category_row = row["product-grid__link href"]
  category_name = category_row.split("/")[6].gsub("-", " ").titleize
  category = Category.find_or_create_by(
    category_name: category_name
  )
  puts "Category name #{category_name}"

  price = !row["product-price-text"].nil? ? row["product-price-text"].gsub("$", "").to_f : 10.99
  quantity = rand(100)
  new_product = category.products.create_or_find_by(
    product_name:        row["product-title-text"],
    product_description: "This is #{row['product-title-text']}",
    product_price:       price,
    stock_quantity:      quantity,
    cart_quantity:       0
  )
  puts "price #{price} quantity #{quantity}"

  img_src = !row["lazy-img src"].nil? ? row["lazy-img src"] : "no_image.png"
  new_product.product_images.create(
    image_title: row["product-title-text"],
    image_path:  img_src
  )
end

# Generate some users
# 10.times
# Faker::Config.locale = "en-CA"
# first_name = Faker::Name.first_name
# last_name = Faker::Name.last_name
# street_address = Faker::Address.street_name
# city = Faker::Address.city
# province = Faker::Address.state
# postal_code = Faker::Address.postcode
# email = "#{first_name + last_name}@email.com"
# Customer.create!(
#   customer_name: "#{first_name} #{last_name}",
#   email:         email,
#   address:       street_address,
#   city:          city,
#   # province_id:      ,
#   postal_code:   postal_code,
#   country:       Faker::Address.country_by_code(code: "CA")
# )

unless AdminUser.exists?(email: "admin@admin.com")
  if Rails.env.development?
    AdminUser.find_or_create_by(email: "admin@admin.com", password: "AdminPassword", password_confirmation: "AdminPassword")
  end
end
