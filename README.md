# Adventure Time Apparel

## Ecommerce site built with Rails

* Built with Ruby v 2.7.1 

### Describe Datasets
- Customer – responsible for holding information and account of customer making the purchase of apparel, this will allow order fulfillment
     - Associations 
	     - has_many (Orders)
         - has_one (Province)
     - Column names
         - Customer_name – string
         - Email – string
         - Password – string
         - Address – string
         - City – string
         - Province – string
         - Postal_code – string
         - Country – string
- Province – contains taxes information for the province that will later be used during order GST and PST calculation
     - Used [Canada’s sale taxes API](https://salestaxapi.ca/)
     - Associations
        -  has_one (Customer)
     - Column names
        - province_name – string
        - GST_rate – string
        - PST_rate – string
- Order – Holds information about a specific order a customer has made as well as the order cost
     - Associations
        - Belongs_to (Customer)
        - Has_many (Products) through OrderProduct
     - Column names
        - Shipping_address – string
        - Order_cost – float
        - Shipping_cost – float
        - GST – float
        - PST – float
- OrderProduct – a join table allowing for order to have multiple products.
     - Associations
        - Belongs_to (Product)
        - Belongs_to (Order)
     - Column names
        - Product_quantity – integer
        - Quantity_price  float
- Product – table containing all information about a product.
     - Associations
        - Belongs_to (Category)
        - Has_many (ProductImage)
        - Has_many (OrderProducts)
        - Has_many (Orders) through OrderProduct
     - Column names
        - Product_name – string
        - product_price – float
        - stock_quantity – integer
- Category – table containing all the existing categories of products
     - Associations
        - Has_many (Product)
     - Column names
        - Category_name – string
- ProductImage – table containing images for products
     - Associations
        - Belongs_to (Product)
     - Column names
        - Image_title – string
        - Image_url – string

        

### ERD Diagram
![ERD Diagram](app/assets/images/FinalProposalERD.png)

### Generating ActiveRecord Models and Tables
    # Create Models
    rails g model Category category_name:string
    rails g model Product product_name:string product_desription:text product_price:float stock_quantity:integer category:references
    rails g model ProductImage image_title:string image_path:string product:references
    rails g model Province province_name:string gst_rate:string pst_rate:string
    rails g model Customer customer_name:string email:string password:string address:string city:string postal_code:string country:string province:references
    rails g model Order shipping_address:string order_cost:float shipping_cost:float gst_amount:float pst_amount:float customer:references
    # Create Relationship Models
    rails g model OrderProduct product_quantity:integer quantity_price:float order:references product:references

### Generating ActiveRecord Models and Tables
   -used ***activeadmin*** and ***devise*** gems for admin dashboard
   
### Built with Bootstrap 4 and Font Awesome 5
    # Run the following code
    yarn add bootstrap jquery popper.js
    yarn add @fortawesome/fontawesome-free
    
   The add the following code to ***application.scss***
   
    @import "bootstrap/scss/bootstrap";
    $fa-font-path: '@fortawesome/fontawesome-free/webfonts';
   
   And this to ***application.js***

    import 'bootstrap'
    import 'jquery'
    import "@fortawesome/fontawesome-free/js/all";