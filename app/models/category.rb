class Category < ApplicationRecord
  has_many :products, dependent: :restrict_with_exception

  def to_s
    category_name
  end
end
