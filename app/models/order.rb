class Order < ApplicationRecord
  belongs_to :customer
  has_many :order_products, dependent: :restrict_with_exception
  has_many :products, through: :order_products
end
