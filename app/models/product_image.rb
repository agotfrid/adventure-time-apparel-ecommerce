class ProductImage < ApplicationRecord
  belongs_to :product
  validates :image_title, presence: true
  has_one_attached :image
end
