class Province < ApplicationRecord
  has_one :customer, dependent: :restrict_with_exception
end
