class Product < ApplicationRecord
  belongs_to :category
  has_many :order_products, dependent: :restrict_with_exception
  has_many :product_images

  validates :product_name, :product_price, :stock_quantity, presence: true

  def to_s
    product_name
  end
end
