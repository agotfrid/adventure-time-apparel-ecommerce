class PagesController < ApplicationController
  def home
  end

  def search
    if params[:category] == "all"
      @products = Product.where("product_name LIKE ?", "%#{params[:product]}%").page(params[:page]).per(15)
    else
      @category = Category.find(params[:category])
      @products = Product.where("product_name LIKE ? AND category_id = ?", "%#{params[:product]}%",
                                params[:category]).page(params[:page]).per(15)
    end
  end

  def contact
  end

  def about
  end
end
