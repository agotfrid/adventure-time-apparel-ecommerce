class ProductsController < ApplicationController
  before_action :initialize_session
  before_action :increment_visit_count, only: [:show]

  def index
    @products = Product.includes(:product_images, :category)
                       .all.page(params[:page]).per(15).order(:product_name)
  end

  def show
    @product = Product.find(params[:id])
  end

  private

  def initialize_session
    session[:visit_count] ||= 0 # Initialize visit count
  end

  def increment_visit_count
    session[:visit_count] += 1 # Increment the count
    @visit_count = session[:visit_count]
  end
end
