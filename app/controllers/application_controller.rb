class ApplicationController < ActionController::Base
  before_action :load_cart

  def add_to_cart
    id = params[:id].to_i
    cart_event = params[:cart_event]

    if cart_event == "+"
      session[:cart].each do |product|
        product.cart_quantity += 1 if product.id == id
      end
    end
    session[:cart] << id unless session[:cart].include?(id)
    redirect_back(fallback_location: root_path) # returns user back to where they were
  end

  def remove_from_cart
    id = params[:id].to_i
    cart_event = params[:cart_event]

    if cart_event.equal?("-")
      session[:cart].each do |product|
        product.cart_quantity -= 1 if product.id.equal?(id)
        session[:cart].delete(id) if product.cart_quantity.zero?
      end
    else
      session[:cart].delete(id)
    end

    redirect_back(fallback_location: root_path) # returns user back to where they were
  end

  private

  def initialize_session
    session[:visit_count] ||= 0 # Initialize visit count
    session[:cart] ||= []
  end

  def load_cart
    @cart = Product.find(session[:cart])
  end

end
