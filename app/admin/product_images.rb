ActiveAdmin.register ProductImage do
  permit_params :image_title, :image_path, :product_id, :image

  # Formtastic
  form do |f|
    f.semantic_errors
    f.inputs
    f.inputs do
      f.input :image, as: :file
    end
    f.actions
  end
end
