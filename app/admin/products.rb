ActiveAdmin.register Product do
  permit_params :product_name, :product_description, :product_price, :stock_quantity, :category_id, :image
end
