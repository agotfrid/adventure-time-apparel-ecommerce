ActiveAdmin.register Order do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :shipping_address, :order_cost, :shipping_cost, :gst_amount, :pst_amount, :customer_id
  #
  # or
  #
  # permit_params do
  #   permitted = [:shipping_address, :order_cost, :shipping_cost, :gst_amount, :pst_amount, :customer_id]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  
end
