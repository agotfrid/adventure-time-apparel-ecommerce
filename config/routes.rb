Rails.application.routes.draw do
  get 'categories', to: "categories#index"
  get 'categories/show'
  get 'products', to: "products#index"
  get 'products/show'
  post 'products/add_to_cart/:id', to: 'products#add_to_cart', as: 'add_to_cart'
  delete 'products/remove_from_cart/:id', to: 'products#remove_from_cart', as: 'remove_from_cart'
  root 'pages#home'
  get 'contact', to: 'pages#contact'
  get 'about', to: 'pages#about'
  get 'search', to: 'pages#search'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  resources :products, :categories, :pages
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
